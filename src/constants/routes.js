export const routes = {
  HOME: '/',
  CART: '/cart',
  PROMOTION: '/promotion',
  ACCOUNT: '/account',
  LOGIN: '/login',
}