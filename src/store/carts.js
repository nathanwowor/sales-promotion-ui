import { createAsyncThunk, createSlice } from '@reduxjs/toolkit'
import request from '../request'
import { alertTypes } from '../components/common/AlertBox'
import { fetchProducts } from './products'

export const fetchCarts = createAsyncThunk(
  'carts/fetch',
  async (_, thunkAPI) => {
    const page = thunkAPI.getState().carts.page
    const query = thunkAPI.getState().carts.query
    const response = await request.get(`carts?page=${page || 1}&query=${query || ''}`)
    return response.data
  },
)

export const addCart = createAsyncThunk(
  'carts/add',
  async (cart, thunkAPI) => {
    const cartItems = thunkAPI.getState().cartItems.data
    const body = { cart, cartItems }
    await request.post('carts', body).then(() => thunkAPI.dispatch(fetchCarts()))
  },
)

export const updateCart = createAsyncThunk(
  'carts/update',
  async ({ documentNumber, ...cart }, thunkAPI) => {
    const cartItems = thunkAPI.getState().cartItems.data
    const body = { cart, cartItems }
    return await request.put(`carts/${documentNumber}`, body).then(response => response.data)
  },
)

export const deleteCart = createAsyncThunk(
  'carts/delete',
  async (documentNumber, thunkAPI) => {
    await request.delete(`carts/${documentNumber}`).then(() => thunkAPI.dispatch(fetchCarts()))
  },
)

export const checkCartPromotions = createAsyncThunk(
  'carts/promotions',
  async (documentNumber) => {
    return await request.put(`carts/${documentNumber}/promotions`).then(response => response.data)
  },
)

export const checkoutCart = createAsyncThunk(
  'carts/checkout',
  async (documentNumber, thunkAPI) => {
    return await request.put(`carts/${documentNumber}/checkout`)
      .then(() => documentNumber)
      .catch(error => {
        thunkAPI.dispatch(fetchProducts())
        return thunkAPI.rejectWithValue(error.response.data)
      })
  },
)

const { reducer, actions } = createSlice({
  name: 'carts',
  initialState: {
    page: 1,
    query: '',
    data: {},
    alert: {
      type: '',
      title: '',
      message: '',
    },
  },
  reducers: {
    setCartPage: (state, { payload }) => {
      state.page = payload
    },
    setCartQuery: (state, { payload }) => {
      state.query = payload
    },
    deleteCartQuery: (state) => {
      state.query = ''
    },
    clearCartAlert: state => {
      state.alert.type = ''
      state.alert.title = ''
      state.alert.message = ''
    },
  },
  extraReducers: {
    [fetchCarts.fulfilled]: (state, { payload }) => {
      if (payload.content.length === 0) {
        state.data = payload
        state.page = payload.totalPages || 1
      } else {
        payload.content.map(cart => cart.netPrice = cart.totalGrossPrice - cart.totalLineDiscount)
        state.data = payload
      }
    },
    [updateCart.fulfilled]: (state, { payload }) => {
      const cart = state.data.content.find(cart => cart.documentNumber === payload.documentNumber)
      for (const p in payload) {
        cart[p] = payload[p]
      }
      state.alert.type = alertTypes.SUCCESS
      state.alert.message = 'Cart updated.'
    },
    [checkCartPromotions.fulfilled]: (state, { payload }) => {
      // backend returns null if no promo applied
      if (payload) {
        const cart = state.data.content.find(cart => cart.documentNumber === payload.documentNumber)
        cart.totalLineDiscount = payload.totalLineDiscount
        state.alert.type = alertTypes.SUCCESS
        state.alert.message = 'Promotion(s) applied.'
      } else {
        state.alert.type = alertTypes.WARNING
        state.alert.message = 'No promotion available.'
      }
    },
    [checkoutCart.fulfilled]: (state, { payload }) => {
      const cart = state.data.content.find(cart => cart.documentNumber === payload)
      cart.isPaid = true
      state.alert.type = alertTypes.SUCCESS
      state.alert.message = 'Cart checkout processed.'
    },
    [checkoutCart.rejected]: (state, { payload }) => {
      state.alert.type = alertTypes.ERROR
      state.alert.message = payload.message
    },
  },
})

export const { setCartPage, setCartQuery, deleteCartQuery, clearCartAlert } = actions
export default reducer