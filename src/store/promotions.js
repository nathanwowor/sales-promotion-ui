import { createAsyncThunk, createSlice } from '@reduxjs/toolkit'
import request from '../request'
import { alertTypes } from '../components/common/AlertBox'

export const fetchPromotions = createAsyncThunk(
  'promotions/fetch',
  async (_, thunkAPI) => {
    const page = thunkAPI.getState().promotions.page
    const filter = thunkAPI.getState().promotions.filter
    const query = thunkAPI.getState().promotions.query
    const response = await request.get(`promotions?page=${page || 1}&filter=${filter || ''}&query=${query || ''}`)
    return response.data
  },
)

export const addPromotion = createAsyncThunk(
  'promotions/add',
  async (body, thunkAPI) => {
    await request.post('promotions', body).then(() => thunkAPI.dispatch(fetchPromotions()))
  },
)

export const updatePromotion = createAsyncThunk(
  'promotions/update',
  async ({ documentNumber, ...body }) => {
    await request.put(`promotions/${documentNumber}`, body)
  },
)

export const deletePromotion = createAsyncThunk(
  'promotions/delete',
  async (documentNumber, thunkAPI) => {
    await request.delete(`promotions/${documentNumber}`).then(() => thunkAPI.dispatch(fetchPromotions()))
  },
)

const { reducer, actions } = createSlice({
  name: 'promotions',
  initialState: {
    page: 1,
    filter: '',
    query: '',
    data: {},
    alert: {
      type: '',
      title: '',
      message: '',
    },
  },
  reducers: {
    setPromotionPage: (state, { payload }) => {
      state.page = payload
    },
    setPromotionFilter: (state, { payload }) => {
      state.filter = payload
    },
    setPromotionQuery: (state, { payload }) => {
      state.query = payload
    },
    clearPromotionAlert: state => {
      state.alert.type = ''
      state.alert.title = ''
      state.alert.message = ''
    },
  },
  extraReducers: {
    [fetchPromotions.fulfilled]: (state, { payload }) => {
      if (payload.content.length === 0) {
        state.data = payload
        state.page = payload.totalPages || 1
      } else {
        state.data = payload
      }
    },
    [updatePromotion.fulfilled]: state => {
      state.alert.type = alertTypes.SUCCESS
      state.alert.message = 'Promotion updated.'
    },
  },
})

export const { setPromotionPage, setPromotionFilter, setPromotionQuery, clearPromotionAlert } = actions
export default reducer