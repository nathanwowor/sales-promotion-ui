import { createAsyncThunk, createSlice } from '@reduxjs/toolkit'
import request from '../request'

export const fetchProducts = createAsyncThunk('products/fetch', async () => {
  const response = await request.get('products')
  return response.data
})

const { reducer } = createSlice({
  name: 'products',
  initialState: {
    data: [],
  },
  extraReducers: {
    [fetchProducts.fulfilled]: (state, { payload }) => {
      state.data = payload
    },
  },
})

export default reducer