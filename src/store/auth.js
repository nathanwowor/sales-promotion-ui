import { createAsyncThunk, createSlice } from '@reduxjs/toolkit'
import request from '../request'
import { alertTypes } from '../components/common/AlertBox'

export const login = createAsyncThunk(
  'auth/login',
  async (values, thunkAPI) => {
    return await request.post('login', values)
      .then(res => res.data)
      .catch(err => thunkAPI.rejectWithValue(err.response.data))
  },
)

const { reducer, actions } = createSlice({
  name: 'auth',
  initialState: {
    token: localStorage.getItem('token') || '',
    alert: {
      type: '',
      title: '',
      message: '',
    },
  },
  reducers: {
    logout: (state) => {
      localStorage.removeItem('token')
      state.token = ''
    },
    clearAuthAlert: state => {
      state.alert.type = ''
      state.alert.title = ''
      state.alert.message = ''
    },
  },
  extraReducers: {
    [login.fulfilled]: (state, { payload }) => {
      localStorage.setItem('token', payload)
      state.token = payload
      state.alert.type = alertTypes.SUCCESS
      state.alert.message = 'Login successful'
    },
    [login.rejected]: (state, { payload }) => {
      state.alert.type = alertTypes.ERROR
      state.alert.message = payload
    },
  },
})

export const { logout, clearAuthAlert } = actions
export default reducer