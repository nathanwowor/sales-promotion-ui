import { createAsyncThunk, createSlice } from '@reduxjs/toolkit'
import request from '../request'

export const fetchCartItems = createAsyncThunk(
  'cartItems/fetch',
  async (documentNumber) => {
    const response = await request.get(`carts/${documentNumber}/items`)
    return response.data
  },
)

const { reducer, actions } = createSlice({
  name: 'cartItems',
  initialState: {
    data: [],
  },
  reducers: {
    addItem: (state, { payload }) => {
      const item = state.data.find(item => item.product?.code === payload.code)
      if (item) {
        if (item.quantity < item.product?.stock) item.quantity += 1
      } else {
        state.data.push({ product: payload, quantity: 1 })
      }
    },
    deleteItem: (state, { payload }) => {
      state.data = state.data.filter(item => item.product?.code !== payload)
    },
    addItemQty: (state, { payload }) => {
      const item = state.data.find(item => item.product?.code === payload)
      if (item.quantity < item.product?.stock) item.quantity += 1
    },
    subtractItemQty: (state, { payload }) => {
      const index = state.data.findIndex(item => item.product?.code === payload)
      state.data[index].quantity -= 1
      if (state.data[index].quantity === 0) {
        state.data = state.data.filter(item => item.product?.code !== payload)
      }
    },
    clearCartItems: (state) => {
      state.data = []
    },
  },
  extraReducers: {
    [fetchCartItems.fulfilled]: (state, { payload }) => {
      state.data = payload
    },
  },
})

export const { addItem, deleteItem, addItemQty, subtractItemQty, clearCartItems } = actions
export default reducer