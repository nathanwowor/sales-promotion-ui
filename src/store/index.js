import { configureStore } from '@reduxjs/toolkit'
import auth from './auth'
import products from './products'
import promotions from './promotions'
import carts from './carts'
import cartItems from './cartItems'

const store = configureStore({
  reducer: {
    auth,
    products,
    promotions,
    carts,
    cartItems,
  },
})

export default store