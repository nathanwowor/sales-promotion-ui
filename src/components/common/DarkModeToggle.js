import React from 'react'
import { IconButton, Tooltip, useColorMode } from '@chakra-ui/react'
import { MoonIcon, SunIcon } from '@chakra-ui/icons'

const label = 'Toggle Dark Mode'

function DarkModeToggle () {
  const { colorMode, toggleColorMode } = useColorMode()

  return (
    <Tooltip label={label}>
      <IconButton
        icon={colorMode === 'light' ? <MoonIcon/> : <SunIcon/>}
        onClick={toggleColorMode}
        aria-label={label}
      />
    </Tooltip>
  )
}

export default DarkModeToggle