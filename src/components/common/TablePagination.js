import React, { useEffect, useState } from 'react'
import { useDispatch } from 'react-redux'
import { ButtonGroup, IconButton, NumberInput, NumberInputField, Stack, Text, Tooltip } from '@chakra-ui/react'
import { ArrowLeftIcon, ArrowRightIcon, ChevronLeftIcon, ChevronRightIcon } from '@chakra-ui/icons'

function TablePagination ({ page, setPage, totalPages }) {
  const dispatch = useDispatch()

  const [pageInput, setPageInput] = useState(page)
  const labels = {
    GOTO_FIRST_PAGE: 'First Page',
    GOTO_PREV_PAGE: 'Previous Page',
    GOTO_NEXT_PAGE: 'Next Page',
    GOTO_LAST_PAGE: 'Last Page',
  }

  useEffect(() => {
    setPageInput(page)
  }, [page])

  function handleChangeInput (value) {
    setPageInput(Number(value) || 0)
  }

  const handleKeyDown = (event) => {
    if (event.key === 'Enter') {
      const value = Math.max(1, Math.min(totalPages, pageInput))
      setPageInput(value)
      dispatch(setPage(value))
    }
  }

  return (
    <Stack isInline justifyContent="center" alignItems="center">
      <ButtonGroup isDisabled={page <= 1}>
        <Tooltip label={labels.GOTO_FIRST_PAGE}>
          <IconButton
            onClick={() => dispatch(setPage(1))}
            icon={<ArrowLeftIcon h={3} w={3}/>}
            aria-label={labels.GOTO_FIRST_PAGE}
          />
        </Tooltip>
        <Tooltip label={labels.GOTO_PREV_PAGE}>
          <IconButton
            onClick={() => dispatch(setPage(page - 1))}
            icon={<ChevronLeftIcon h={6} w={6}/>}
            aria-label={labels.GOTO_PREV_PAGE}
          />
        </Tooltip>
      </ButtonGroup>

      <Text>Page</Text>
      <NumberInput
        width="5rem"
        value={pageInput}
        min={1} max={totalPages || 1}
        onChange={handleChangeInput}
        onKeyDown={handleKeyDown}
      >
        <NumberInputField paddingRight={4}/>
      </NumberInput>
      <Text>of {totalPages || 1}</Text>

      <ButtonGroup isDisabled={page >= totalPages}>
        <Tooltip label={labels.GOTO_NEXT_PAGE}>
          <IconButton
            onClick={() => dispatch(setPage(page + 1))}
            icon={<ChevronRightIcon h={6} w={6}/>}
            aria-label={labels.GOTO_NEXT_PAGE}
          />
        </Tooltip>
        <Tooltip label={labels.GOTO_LAST_PAGE}>
          <IconButton
            onClick={() => dispatch(setPage(totalPages))}
            icon={<ArrowRightIcon h={3} w={3}/>}
            aria-label={labels.GOTO_LAST_PAGE}
          />
        </Tooltip>
      </ButtonGroup>
    </Stack>
  )
}

export default TablePagination