import React from 'react'
import { useNavigate } from 'react-router-dom'
import { Tab, TabList, Tabs, Tooltip } from '@chakra-ui/react'
import { routes } from '../../../constants/routes'

function NavTabs ({ index }) {
  // useNavigate because Link component broke Tab height
  const navigate = useNavigate()

  function handleClickHome () {
    navigate(routes.HOME)
  }

  function handleClickPromotion () {
    navigate(routes.PROMOTION)
  }

  function handleClickCart () {
    navigate(routes.CART)
  }

  return (
    <Tabs defaultIndex={index} size="lg">
      <TabList>
        <Tooltip label="Home Page">
          <Tab onClick={handleClickHome}><i className="fa-solid fa-house"/></Tab>
        </Tooltip>
        <Tab onClick={handleClickPromotion}>Promotion Settings</Tab>
        <Tab onClick={handleClickCart}>Checkout Carts</Tab>
      </TabList>
    </Tabs>
  )
}

export default NavTabs