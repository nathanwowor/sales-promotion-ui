import React, { useEffect } from 'react'
import { ButtonGroup, HStack } from '@chakra-ui/react'
import NavTabs from './NavTabs'
import DarkModeToggle from '../DarkModeToggle'
import UserMenu from './UserMenu'
import { useDispatch, useSelector } from 'react-redux'
import { fetchProducts } from '../../../store/products'
import { routes } from '../../../constants/routes'

function Header ({ index }) {
  const dispatch = useDispatch()
  const token = useSelector(state => state.auth.token)

  useEffect(() => {
    if (!token) window.location.assign(routes.LOGIN)
  }, [token])

  useEffect(() => {
    dispatch(fetchProducts())
  }, [dispatch])

  return (
    <HStack justifyContent="space-between" alignItems="flex-start">
      <NavTabs index={index}/>
      <ButtonGroup>
        <DarkModeToggle/>
        <UserMenu/>
      </ButtonGroup>
    </HStack>
  )
}

export default Header