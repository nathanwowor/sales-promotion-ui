import React from 'react'
import { useDispatch } from 'react-redux'
import { IconButton, Menu, MenuButton, MenuItem, MenuList, Tooltip } from '@chakra-ui/react'
import { logout } from '../../../store/auth'
import { routes } from '../../../constants/routes'

// todo: display logged-in user's name

function UserMenu () {
  const dispatch = useDispatch()

  function handleLogout () {
    dispatch(logout())
  }

  return (
    <div> {/* wrapped in div to avoid popper warning when used as Stack or ButtonGroup child */}
      <Menu>
        {/* DISPLAYED BUTTON */}
        <Tooltip label="User Menu">
          <MenuButton as={IconButton} icon={<i className="fa-solid fa-user"/>}/>
        </Tooltip>

        {/* DROP-DOWN MENU */}
        <MenuList minWidth="8rem"> {/* override default minWidth */}
          {/*<Link to='/account'>*/}
          {/*  <MenuItem>Account</MenuItem>*/}
          {/*</Link>*/}
          <a href={routes.LOGIN}>
            <MenuItem onClick={handleLogout}>Logout</MenuItem>
          </a>
        </MenuList>
      </Menu>
    </div>
  )
}

export default UserMenu