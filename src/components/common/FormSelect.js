import React from 'react'
import { FormControl, FormErrorMessage, FormLabel, InputGroup, Select } from '@chakra-ui/react'
import { Field, useField } from 'formik'

function FormSelect ({ children, label, ...props }) {
  const [field, meta] = useField(props)

  return (
    <FormControl isInvalid={meta.touched && meta.error}>
      <FormLabel>{label}</FormLabel>
      <InputGroup>
        <Field as={Select} {...props} {...field}>
          <option value="" hidden disabled>Select option</option>
          {children}
        </Field>
      </InputGroup>
      <FormErrorMessage>{meta.error}</FormErrorMessage>
    </FormControl>
  )
}

export default FormSelect