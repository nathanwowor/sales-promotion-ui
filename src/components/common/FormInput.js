import React from 'react'
import {
  FormControl,
  FormErrorMessage,
  FormLabel,
  Input,
  InputGroup,
  InputLeftAddon,
  InputRightAddon,
} from '@chakra-ui/react'
import { Field, useField } from 'formik'

function FormInput ({ label, leftAddon, rightAddon, ...props }) {
  const [field, meta] = useField(props)

  return (
    <FormControl isInvalid={meta.touched && meta.error}>
      <FormLabel>{label}</FormLabel>
      <InputGroup>
        {leftAddon && <InputLeftAddon children={leftAddon}/>}
        <Field as={Input} {...props} {...field}/>
        {rightAddon && <InputRightAddon children={rightAddon}/>}
      </InputGroup>
      <FormErrorMessage>{meta.error}</FormErrorMessage>
    </FormControl>
  )
}

export default FormInput