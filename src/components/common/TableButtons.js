import React, { useRef } from 'react'
import { useNavigate } from 'react-router-dom'
import {
  AlertDialog,
  AlertDialogBody,
  AlertDialogContent,
  AlertDialogFooter,
  AlertDialogHeader,
  AlertDialogOverlay,
  Button,
  ButtonGroup,
  IconButton,
  Tooltip,
  useDisclosure,
} from '@chakra-ui/react'
import { DeleteIcon, EditIcon, InfoIcon } from '@chakra-ui/icons'
import { routes } from '../../constants/routes'

const labels = {
  PRINT: 'Print Invoice', // only for cart details
  VIEW: 'View Details',
  EDIT: 'Edit',
  DELETE: 'Delete',
}

function TableButtons ({ route, documentNumber, editable, handleDelete }) {
  const { isOpen, onOpen, onClose } = useDisclosure()
  const cancelRef = useRef()
  const navigate = useNavigate()
  const handleView = () => navigate(`${route}/${documentNumber}/view`)
  const handleEdit = () => navigate(`${route}/${documentNumber}/edit`)

  function handlePrint () {
    window.open(`http://localhost:8080/carts/${documentNumber}/pdf`, '_blank')
    return false
  }

  function handleDeleteConfirm () {
    handleDelete()
    onClose()
  }

  return (
    <ButtonGroup size="sm">
      {route === routes.CART &&
        <Tooltip label={labels.PRINT}>
          <IconButton
            icon={<i className="fa-solid fa-print"/>}
            aria-label={labels.PRINT}
            onClick={handlePrint}
          />
        </Tooltip>
      }
      <Tooltip label={labels.VIEW}>
        <IconButton
          icon={<InfoIcon/>}
          aria-label={labels.VIEW}
          onClick={handleView}
        />
      </Tooltip>
      <Tooltip label={labels.EDIT}>
        <IconButton
          icon={<EditIcon/>}
          aria-label={labels.EDIT}
          onClick={handleEdit}
          disabled={!editable}
        />
      </Tooltip>
      <Tooltip label={labels.DELETE}>
        <IconButton
          icon={<DeleteIcon color="red.500"/>}
          aria-label={labels.DELETE}
          onClick={onOpen}
          disabled={!editable}
        />
      </Tooltip>
      <AlertDialog isOpen={isOpen} leastDestructiveRef={cancelRef} onClose={onClose}>
        <AlertDialogOverlay>
          <AlertDialogContent>
            <AlertDialogHeader fontSize="lg" fontWeight="bold">
              Delete {route === routes.CART ? 'Cart' : 'Promotion'} "{documentNumber}"
            </AlertDialogHeader>
            <AlertDialogBody>
              Are you sure?
            </AlertDialogBody>
            <AlertDialogFooter>
              <Button ref={cancelRef} onClick={onClose}>
                Cancel
              </Button>
              <Button colorScheme="red" onClick={handleDeleteConfirm} ml={3}>
                Delete
              </Button>
            </AlertDialogFooter>
          </AlertDialogContent>
        </AlertDialogOverlay>
      </AlertDialog>
    </ButtonGroup>
  )
}

export default TableButtons