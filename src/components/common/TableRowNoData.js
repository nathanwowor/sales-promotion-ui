import React from 'react'
import { Th, Tr } from '@chakra-ui/react'

function TableRowNoData () {
  return (
    <Tr><Th padding={4} textAlign="center">NO DATA</Th></Tr>
  )
}

export default TableRowNoData