import React, { useEffect } from 'react'
import { Alert, AlertDescription, AlertIcon, AlertTitle, CloseButton, useDisclosure } from '@chakra-ui/react'
import { useDispatch } from 'react-redux'

export const alertTypes = {
  SUCCESS: 'success',
  ERROR: 'error',
  WARNING: 'warning',
  INFO: 'info',
}

const alertTitles = {
  success: 'Success!',
  error: 'Error!',
  warning: 'Warning!',
  info: 'Info!',
}

function AlertBox ({ alert, clearAlert }) {
  const dispatch = useDispatch()
  const { isOpen, onClose, onOpen } = useDisclosure()

  useEffect(() => {
    if (alert.message) {
      onOpen()
    } else {
      onClose()
    }
  }, [alert, onClose, onOpen])

  useEffect(() => {
    return () => {
      if (clearAlert) dispatch(clearAlert())
    }
  }, [clearAlert, dispatch])

  function handleClose () {
    if (clearAlert) dispatch(clearAlert())
    onClose()
  }

  return (
    <>
      {isOpen && alert.type &&
        <Alert status={alert.type} textAlign="left">
          <AlertIcon/>
          <AlertTitle>{alert.title || alertTitles[alert.type]}</AlertTitle>
          <AlertDescription width="full">{alert.message}</AlertDescription>
          <CloseButton onClick={handleClose}/>
        </Alert>
      }
    </>
  )
}

export default AlertBox