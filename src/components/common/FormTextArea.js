import React from 'react'
import { FormControl, FormErrorMessage, FormLabel, Textarea } from '@chakra-ui/react'
import { Field, useField } from 'formik'

function FormTextArea ({ label, ...props }) {
  const [field, meta] = useField(props)

  return (
    <FormControl isInvalid={meta.touched && meta.error}>
      <FormLabel>{label}</FormLabel>
      <Field as={Textarea} {...props} {...field} />
      <FormErrorMessage>{meta.error}</FormErrorMessage>
    </FormControl>
  )
}

export default FormTextArea