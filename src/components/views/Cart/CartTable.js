import React from 'react'
import { Table, TableContainer, Tbody, Th, Thead, Tr } from '@chakra-ui/react'
import CartTableRow from './CartTableRow'
import TableRowNoData from '../../common/TableRowNoData'

function CartTable ({ data }) {
  return (
    <TableContainer borderWidth={2} borderRadius={8}>
      <Table size="sm">
        {data?.length === 0 ? <TableRowNoData/> : (
          <Thead>
            <Tr>
              <Th>Number</Th>
              <Th>Description</Th>
              <Th>Order Date</Th>
              <Th isNumeric>Net Amount</Th>
              <Th>Actions</Th>
            </Tr>
          </Thead>
        )}
        <Tbody>
          {data?.map(cart => <CartTableRow key={cart.documentNumber} data={cart}/>)}
        </Tbody>
      </Table>
    </TableContainer>
  )
}

export default CartTable