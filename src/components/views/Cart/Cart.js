import React, { useEffect } from 'react'
import { Link } from 'react-router-dom'
import { useDispatch, useSelector } from 'react-redux'
import { Button, Stack } from '@chakra-ui/react'
import Header from '../../common/Header'
import CartSearchBar from './CartSearchBar'
import CartTable from './CartTable'
import TablePagination from '../../common/TablePagination'
import { fetchCarts, setCartPage } from '../../../store/carts'
import { routes } from '../../../constants/routes'

function Cart () {
  const dispatch = useDispatch()
  const data = useSelector(state => state.carts.data)
  const page = useSelector(state => state.carts.page)
  const query = useSelector(state => state.carts.query)

  useEffect(() => {
    dispatch(fetchCarts())
  }, [dispatch, page, query])
  return (
    <Stack id="app">
      <Header index={2}/>
      <Stack>
        <Stack isInline justifyContent="space-between" alignItems="flex-end">
          <CartSearchBar/>
          <Link to={`${routes.CART}/new`}>
            <Button width="7rem">Add Order</Button>
          </Link>
        </Stack>
        <CartTable data={data.content}/>
        <TablePagination page={page} setPage={setCartPage} totalPages={data.totalPages}/>
      </Stack>
    </Stack>
  )
}

export default Cart