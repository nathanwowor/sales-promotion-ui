import React from 'react'
import { Td, Tooltip, Tr } from '@chakra-ui/react'
import TableButtons from '../../common/TableButtons'
import { routes } from '../../../constants/routes'
import { deleteCart } from '../../../store/carts'
import { useDispatch } from 'react-redux'

function CartTableRow ({ data }) {
  const dispatch = useDispatch()

  function handleDelete () {
    dispatch(deleteCart(data.documentNumber))
  }

  return (
    <Tr key={data.id} _hover={{ background: '#c0c0c03f' }}>
      <Td>{data.documentNumber}</Td>
      <Td>
        <Tooltip label={data.description}>
          {data.description}
        </Tooltip>
      </Td>
      <Td>
        {new Date(data.dateCreated).toLocaleDateString('id-ID', {
          day: '2-digit',
          month: 'short',
          year: 'numeric',
        })}
      </Td>
      <Td isNumeric>{data.netPrice.toLocaleString('id-ID', { style: 'currency', currency: 'IDR' })}</Td>
      <Td width="11.5rem">
        <TableButtons route={routes.CART} documentNumber={data.documentNumber} editable={!data.isPaid} handleDelete={handleDelete}/>
      </Td>
    </Tr>
  )
}

export default CartTableRow