import React, { useEffect, useState } from 'react'
import { HStack, IconButton, Input, InputGroup, InputLeftAddon, InputRightElement, Tooltip } from '@chakra-ui/react'
import { CloseIcon, SearchIcon } from '@chakra-ui/icons'
import FilterMenu from './FilterMenu'
import { useDispatch, useSelector } from 'react-redux'
import { deleteCartQuery, setCartQuery } from '../../../store/carts'

function CartSearchBar () {
  const dispatch = useDispatch()
  const query = useSelector(state => state.carts.query)
  const [queryInput, setQueryInput] = useState('')

  useEffect(() => {
    setQueryInput(query)
  }, [query])

  function handleChange (event) {
    setQueryInput(event.target.value)
  }

  const handleKeyDown = (event) => {
    if (event.key === 'Enter') dispatch(setCartQuery(queryInput))
  }

  function handleReset () {
    dispatch(deleteCartQuery())
  }

  return (
    <HStack>
      <InputGroup width="min(full, 30rem)">
        <InputLeftAddon padding={0} bg="">
          <FilterMenu/>
        </InputLeftAddon>
        <Input
          value={queryInput}
          placeholder="Search"
          onChange={handleChange}
          onKeyDown={handleKeyDown}
          autoComplete="off"
        />
        <InputRightElement padding={0}>
          <Tooltip label="Search">
            <IconButton
              type="submit"
              icon={<SearchIcon/>}
              aria-label="Submit Search Query"
              borderLeftRadius={0}
            />
          </Tooltip>
        </InputRightElement>
      </InputGroup>
      {query &&
        <Tooltip label="Reset Search">
          <IconButton
            icon={<CloseIcon/>}
            aria-label="Reset Search"
            onClick={handleReset}
          />
        </Tooltip>
      }
    </HStack>
  )
}

export default CartSearchBar