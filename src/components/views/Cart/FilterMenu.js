import React, { useState } from 'react'
import { Button, Menu, MenuButton, MenuItemOption, MenuList, MenuOptionGroup } from '@chakra-ui/react'
import { ChevronDownIcon } from '@chakra-ui/icons'

const filters = {
  DOCUMENT_NUMBER: 'Document Number',
}

function FilterMenu () {
  const [filter, setFilter] = useState(filters.DOCUMENT_NUMBER)

  function handleFilterChange (value) {
    setFilter(value)
  }

  return (
    <Menu>
      <MenuButton as={Button} rightIcon={<ChevronDownIcon/>} textAlign="left" width="13rem" borderRightRadius={0}>
        {filter}
      </MenuButton>
      <MenuList>
        <MenuOptionGroup onChange={handleFilterChange} defaultValue={filters.DOCUMENT_NUMBER} type="radio">
          <MenuItemOption value={filters.DOCUMENT_NUMBER}>{filters.DOCUMENT_NUMBER}</MenuItemOption>
        </MenuOptionGroup>
      </MenuList>
    </Menu>
  )
}

export default FilterMenu