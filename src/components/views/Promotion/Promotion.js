import React, { useEffect } from 'react'
import { Button, Stack } from '@chakra-ui/react'
import Header from '../../common/Header'
import PromotionSearchBar from './PromotionSearchBar'
import PromotionTable from './PromotionTable'
import TablePagination from '../../common/TablePagination'
import { useDispatch, useSelector } from 'react-redux'
import { fetchPromotions, setPromotionPage } from '../../../store/promotions'
import { Link } from 'react-router-dom'
import { routes } from '../../../constants/routes'

function Promotion () {
  const dispatch = useDispatch()
  const data = useSelector(state => state.promotions.data)
  const page = useSelector(state => state.promotions.page)
  const filter = useSelector(state => state.promotions.filter)
  const query = useSelector(state => state.promotions.query)

  useEffect(() => {
    dispatch(fetchPromotions())
  }, [dispatch, filter, page, query])

  return (
    <Stack id="app">
      <Header index={1}/>
      <Stack>
        <Stack isInline justifyContent="space-between" alignItems="flex-end">
          <PromotionSearchBar/>
          <Link to={`${routes.PROMOTION}/new`}>
            <Button width="7rem">Add Promo</Button>
          </Link>
        </Stack>
        <PromotionTable data={data.content}/>
        <TablePagination page={page} setPage={setPromotionPage} totalPages={data.totalPages}/>
      </Stack>
    </Stack>
  )
}

export default Promotion