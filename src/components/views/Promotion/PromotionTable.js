import React from 'react'
import { Table, TableContainer, Tbody, Th, Thead, Tr } from '@chakra-ui/react'
import PromotionTableRow from './PromotionTableRow'
import TableRowNoData from '../../common/TableRowNoData'

function PromotionTable ({ data }) {
  return (
    <TableContainer borderWidth={2} borderRadius={8}>
      <Table size="sm">
        <Thead>
          {data?.length === 0 ? <TableRowNoData/> : (
            <Tr>
              <Th>Number</Th>
              <Th>Description</Th>
              <Th>Valid Period</Th>
              <Th>Date Created</Th>
              <Th>Actions</Th>
            </Tr>
          )}
        </Thead>
        <Tbody>
          {data?.map(promotion => <PromotionTableRow key={promotion.documentNumber} data={promotion}/>)}
        </Tbody>
      </Table>
    </TableContainer>
  )
}

export default PromotionTable