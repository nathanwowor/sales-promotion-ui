import React, { useState } from 'react'
import { useDispatch, useSelector } from 'react-redux'
import {
  Button,
  HStack,
  IconButton,
  Input,
  InputGroup,
  InputLeftAddon,
  InputRightElement,
  Menu,
  MenuButton,
  MenuItemOption,
  MenuList,
  MenuOptionGroup,
  Tooltip,
} from '@chakra-ui/react'
import { ChevronDownIcon, CloseIcon, SearchIcon } from '@chakra-ui/icons'
import { setPromotionFilter, setPromotionQuery } from '../../../store/promotions'

function PromotionSearchBar () {
  const dispatch = useDispatch()
  const filters = ['Document Number', 'Product Code']
  const filter = useSelector(state => state.promotions.filter)
  const [filterInput, setFilterInput] = useState(filter || filters[0])
  const query = useSelector(state => state.promotions.query)
  const [queryInput, setQueryInput] = useState(query)

  function handleFilterChange (value) {
    setFilterInput(value)
  }

  function handleQueryChange (event) {
    setQueryInput(event.target.value)
  }

  const handleKeyDown = (event) => {
    if (event.key === 'Enter') handleSubmit()
  }

  function handleSubmit () {
    dispatch(setPromotionQuery(queryInput))
    dispatch(setPromotionFilter(filterInput))
  }

  function handleReset () {
    setQueryInput('')
    dispatch(setPromotionQuery(''))
  }

  return (
    <HStack>
      <InputGroup width="min(full, 30rem)">
        <InputLeftAddon padding={0} bg="">
          <Menu>
            <MenuButton as={Button} rightIcon={<ChevronDownIcon/>} textAlign="left" width="13rem" borderRightRadius={0}>
              {filterInput}
            </MenuButton>
            <MenuList>
              <MenuOptionGroup onChange={handleFilterChange} defaultValue={filterInput} type="radio">
                {filters.map(option => <MenuItemOption key={option} value={option}>{option}</MenuItemOption>)}
              </MenuOptionGroup>
            </MenuList>
          </Menu>
        </InputLeftAddon>
        <Input
          placeholder="Search"
          value={queryInput}
          onChange={handleQueryChange}
          onKeyDown={handleKeyDown}
          autoComplete="off"
        />
        <InputRightElement padding={0}>
          <IconButton
            type="submit"
            icon={<SearchIcon/>}
            aria-label="Search Promotion"
            borderLeftRadius={0}
            onClick={handleSubmit}
          />
        </InputRightElement>
      </InputGroup>
      {query &&
        <Tooltip label="Reset Search">
          <IconButton
            icon={<CloseIcon/>}
            aria-label="Reset Search"
            onClick={handleReset}
          />
        </Tooltip>
      }
    </HStack>
  )
}

export default PromotionSearchBar