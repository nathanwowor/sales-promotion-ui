import React from 'react'
import { useDispatch } from 'react-redux'
import { Td, Tooltip, Tr } from '@chakra-ui/react'
import TableButtons from '../../common/TableButtons'
import { routes } from '../../../constants/routes'
import { deletePromotion } from '../../../store/promotions'

function PromotionTableRow ({ data }) {
  const dispatch = useDispatch()

  function handleDelete () {
    dispatch(deletePromotion(data.documentNumber))
  }

  return (
    <Tr key={data.id}>
      <Td>{data?.documentNumber}</Td>
      <Td>
        <Tooltip label={data.description}>
          {data.description}
        </Tooltip>
      </Td>
      <Td>
        {new Date(data.startDate).toLocaleDateString('id-ID', {
          day: '2-digit',
          month: 'short',
          year: 'numeric',
        })}
        &nbsp;-&nbsp;
        {new Date(data.endDate).toLocaleDateString('id-ID', {
          day: '2-digit',
          month: 'short',
          year: 'numeric',
        })}
      </Td>
      <Td>
        {new Date(data.dateCreated).toLocaleDateString('id-ID', {
          day: '2-digit',
          month: 'short',
          year: 'numeric',
        })}
      </Td>
      <Td width="8rem">
        <TableButtons route={routes.PROMOTION} documentNumber={data.documentNumber} editable={true} handleDelete={handleDelete}/>
      </Td>
    </Tr>
  )
}

export default PromotionTableRow