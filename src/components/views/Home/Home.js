import React from 'react'
import { Link } from 'react-router-dom'
import { Button, Heading, HStack, Stack, VStack } from '@chakra-ui/react'
import Header from '../../common/Header'
import { routes } from '../../../constants/routes'

function Home () {
  return (
    <Stack id="app">
      <Header index={0}/>
      <HStack flexGrow="1" justifyContent="space-evenly" alignItems="center">
        <Button as={Link} to={routes.PROMOTION} h="20rem" w="20rem" borderRadius="3rem" aria-label="Promotions Page">
          <VStack gridRowGap={8}>
            <i className="fa-solid fa-percent fa-10x"/>
            <Heading size="lg">Promotions</Heading>
          </VStack>
        </Button>
        <Button as={Link} to={routes.CART} h="20rem" w="20rem" borderRadius="3rem" aria-label="Cart Page Button">
          <VStack gridRowGap={8}>
            <i className="fa-solid fa-cart-shopping fa-10x"/>
            <Heading size="lg">Carts</Heading>
          </VStack>
        </Button>
      </HStack>
    </Stack>
  )
}

export default Home