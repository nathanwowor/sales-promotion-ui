// TODO: replace formik with react-hook-form
// https://blog.logrocket.com/react-hook-form-vs-formik-comparison/

import React, { useEffect } from 'react'
import { useNavigate } from 'react-router-dom'
import { useDispatch, useSelector } from 'react-redux'
import { Box, Heading, HStack, Stack, VStack } from '@chakra-ui/react'
import { Form, Formik } from 'formik'
import * as Yup from 'yup'
import FormInput from '../../common/FormInput'
import FormTextArea from '../../common/FormTextArea'
import FormSelect from '../../common/FormSelect'
import { addPromotion, updatePromotion } from '../../../store/promotions'
import { routes } from '../../../constants/routes'

const PromotionDetailsForm = ({ documentNumber, edit }) => {
  const dispatch = useDispatch()
  const navigate = useNavigate()
  const data = useSelector(state => state.promotions.data.content?.find(promo => promo.documentNumber === documentNumber))
  const productCodes = useSelector(state => state.products.data.map(product => product.code))

  useEffect(() => {
    if (documentNumber && !data) navigate(routes.PROMOTION)
  }, [data, documentNumber, navigate])

  const today = new Date().toISOString().split('T')[0]

  const initialValues = {
    // initialize properties to avoid undefined values
    documentNumber: '-',
    startDate: today,
    endDate: today,
    description: '',
    forPurchaseProductCode: '',
    forPurchasePrice: '',
    type: 'DISCOUNT',
    discount: '',
    discountLimit: '',
    freeProductCode: '',
    freeProductQuantity: '',
    ...data,
  }

  const validationSchema = Yup.object({
    description: Yup.string().max(255, 'Max 255 characters'),
    forPurchaseProductCode: Yup.string().required('Required')
      .test('productCodeCheck', 'Invalid product code', value => productCodes.includes(value?.toUpperCase())),
    forPurchasePrice: Yup.number().min(0, 'Invalid value').typeError('Invalid value'),
    type: Yup.string().required(),
    discount: Yup.number().when('type', {
      is: type => type === 'DISCOUNT',
      then: Yup.number().required('Required').min(0, 'Invalid value').max(80, 'Discount cannot exceed 80%'),
    }),
    discountLimit: Yup.number().when('type', {
      is: type => type === 'DISCOUNT',
      then: Yup.number().required('Required').min(0, 'Invalid value').typeError('Invalid value'),
    }),
    freeProductCode: Yup.string().when('type', {
      is: type => type === 'FREE_GOODS',
      then: Yup.string().required('Required').test('productCodeCheck', 'Invalid product code', value => productCodes.includes(value?.toUpperCase())),
    }),
    freeProductQuantity: Yup.number().when('type', {
      is: type => type === 'FREE_GOODS',
      then: Yup.number().required('Required').min(0, 'Invalid value').typeError('Invalid value'),
    }),
  })

  function handleSubmit (values) {
    const body = {
      ...values,
      forPurchaseProductCode: values.forPurchaseProductCode.toUpperCase(),
      freeProductCode: values.freeProductCode.toUpperCase(),
    }
    if (documentNumber) {
      dispatch(updatePromotion(body))
      navigate(`${routes.PROMOTION}/${documentNumber}/view`)
    } else {
      dispatch(addPromotion(body))
      navigate(routes.PROMOTION)
    }
  }

  function handleReset () {
    if (documentNumber) {
      navigate(`${routes.PROMOTION}/${documentNumber}/view`)
    } else {
      navigate(routes.PROMOTION)
    }
  }

  return (
    <Box overflow="scroll" borderWidth="2px" rounded="lg" padding={2}>
      <Formik initialValues={initialValues} validationSchema={validationSchema} onSubmit={handleSubmit} onReset={handleReset}>
        {formik => (
          <Form id="promo-form">
            <fieldset disabled={!edit}>
              <VStack gridRowGap={4}>
                <VStack width="full" borderWidth="1px" rounded="lg" padding={2}>
                  <Heading size="lg" marginBottom={2}>PROMOTION HEADER</Heading>
                  <Stack direction={['column', 'column', 'row']} width="full">
                    <VStack>
                      <FormInput
                        type="text"
                        name="documentNumber"
                        label="Invoice Number"
                        disabled={!documentNumber}
                        readOnly
                      />
                      <HStack width="full">
                        <FormInput type="date" name="startDate" label="Start Date" readOnly={!edit}/>
                        <FormInput type="date" name="endDate" label="End Date" readOnly={!edit}/>
                      </HStack>
                    </VStack>
                    <FormTextArea
                      name="description" label="Description"
                      minHeight="7.5rem"
                      resize="none"
                      readOnly={!edit}
                    />
                  </Stack>
                </VStack>

                <Stack direction={['column', 'row']} width="full" alignItems="flex-start" gridRowGap={4}>
                  <Box width="full" borderWidth="1px" rounded="lg" textAlign="center">
                    <Heading size="lg" margin={4}>WHAT CUSTOMERS GET</Heading>
                    <VStack padding={4}>
                      <FormSelect name="type" label="Promotion Type">
                        <option value="DISCOUNT">Discount</option>
                        <option value="FREE_GOODS">Free Goods</option>
                      </FormSelect>
                      {formik.values.type === 'DISCOUNT' &&
                        <>
                          <FormInput
                            type="number" min="0" max="80"
                            name="discount"
                            label="Discount"
                            readOnly={!edit}
                            borderRightRadius="0"
                            rightAddon="%"
                          />
                          <FormInput
                            type="number" min="0"
                            name="discountLimit"
                            label="Max. Discount"
                            readOnly={!edit}
                            borderLeftRadius="0"
                            leftAddon="Rp"
                          />
                        </>
                      }
                      {formik.values.type === 'FREE_GOODS' &&
                        <>
                          <FormInput
                            type="text"
                            name="freeProductCode"
                            label="Product Code"
                            textTransform="uppercase"
                            readOnly={!edit}
                          />
                          <FormInput
                            type="number" min="0"
                            name="freeProductQuantity"
                            label="Quantity"
                            readOnly={!edit}
                          />
                        </>
                      }
                    </VStack>
                  </Box>

                  <Box width="full" borderWidth="1px" rounded="lg" textAlign="center">
                    <Heading size="lg" margin={4}>WHAT CUSTOMERS BUY</Heading>
                    <VStack padding={4}>
                      <FormInput
                        type="text"
                        name="forPurchaseProductCode"
                        label="Product Code"
                        textTransform="uppercase"
                        readOnly={!edit}
                      />
                      <FormInput
                        type="number" min="0"
                        name="forPurchasePrice"
                        label="Min. Purchase"
                        readOnly={!edit}
                        borderLeftRadius="0"
                        leftAddon="Rp"
                      />
                    </VStack>
                  </Box>
                </Stack>
              </VStack>

              {/*/!*for development*!/*/}
              {/*<Box as="pre" marginTop={10} overflow="hidden" textOverflow="ellipsis">*/}
              {/*  "values": {JSON.stringify({ ...formik.values }, null, 2)}*/}
              {/*  <br/>*/}
              {/*  "errors": {JSON.stringify(formik.errors, null, 2)}*/}
              {/*</Box>*/}
            </fieldset>
          </Form>
        )}
      </Formik>
    </Box>
  )
}

export default PromotionDetailsForm
