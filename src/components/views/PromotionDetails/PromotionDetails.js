import React from 'react'
import { Link, useNavigate, useParams } from 'react-router-dom'
import { useDispatch, useSelector } from 'react-redux'
import { Button, ButtonGroup, Stack } from '@chakra-ui/react'
import Header from '../../common/Header'
import PromotionDetailsForm from './PromotionDetailsForm'
import { routes } from '../../../constants/routes'
import AlertBox from '../../common/AlertBox'
import { clearPromotionAlert } from '../../../store/promotions'

function PromotionDetails ({ edit }) {
  const dispatch = useDispatch()
  const navigate = useNavigate()
  const { documentNumber } = useParams()
  const alert = useSelector(state => state.promotions.alert)

  function handleEdit () {
    dispatch(clearPromotionAlert())
    navigate(`${routes.PROMOTION}/${documentNumber}/edit`)
  }

  return (
    <Stack id="app">
      <Header index={1}/>
      <Stack isInline justifyContent="space-between" alignItems="flex-end">
        <Link to={routes.PROMOTION}>
          <Button width="4rem">Back</Button>
        </Link>
        {edit ? (
          <ButtonGroup key="1" colorScheme="blue">
            <Button form="promo-form" type="submit" width="4rem">Save</Button>
            <Button form="promo-form" type="reset" width="4rem" variant="outline">Cancel</Button>
          </ButtonGroup>
        ) : (
          <ButtonGroup key="2">
            <Button width="4rem" onClick={handleEdit}>Edit</Button>
          </ButtonGroup>
        )}
      </Stack>
      <AlertBox alert={alert} clearAlert={clearPromotionAlert}/>
      <PromotionDetailsForm documentNumber={documentNumber} edit={edit}/>
    </Stack>
  )
}

export default PromotionDetails