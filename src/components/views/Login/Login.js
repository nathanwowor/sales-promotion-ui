import React, { useEffect } from 'react'
import { useSelector } from 'react-redux'
import { Box, Flex, Image } from '@chakra-ui/react'
import DarkModeToggle from '../../common/DarkModeToggle'
import LoginForm from './LoginForm'
import loginImage from '../../../images/nexsoft-building.jpg'
import { routes } from '../../../constants/routes'

function Login () {
  const token = useSelector(state => state.auth.token)

  useEffect(() => {
    if (token) {
      setTimeout(function () {
        window.location.assign(routes.HOME)
      }, 1500)
    }
  }, [token])

  return (
    // login page will follow screen size, but won't shrink below 30x30rem (login form size)
    <Flex height="max(100vh, 30em)" width="max(100vw, 30em)">
      {/* LEFT SECTION: IMAGE */}
      <Box flexGrow="1">
        <Image
          height="full" width="full"
          objectFit="cover"
          src={loginImage}
          alt="NexSoft Building"
        />
      </Box>

      {/* RIGHT SECTION: LOGIN FORM */}
      <Box flexShrink="0" height="30rem" width="30rem" textAlign="center">
        <Box textAlign="right" margin={2}>
          <DarkModeToggle/>
        </Box>
        <Box margin={16}>
          <LoginForm/>
        </Box>
      </Box>
    </Flex>
  )
}

export default Login