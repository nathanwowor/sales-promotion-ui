import React from 'react'
import { useDispatch, useSelector } from 'react-redux'
import { Button, Heading, Stack } from '@chakra-ui/react'
import { Form, Formik } from 'formik'
import * as yup from 'yup'
import FormInput from '../../common/FormInput'
import AlertBox from '../../common/AlertBox'
import { clearAuthAlert, login } from '../../../store/auth'

function LoginForm () {
  const dispatch = useDispatch()
  const alert = useSelector(state => state.auth.alert)

  const initialValues = { email: '', password: '' }
  const validationSchema = yup.object({
    email: yup.string().required('Required'),
    password: yup.string().required('Required'),
  })

  function handleSubmit (values) {
    dispatch(login(values))
  }

  return (
    <Formik initialValues={initialValues} validationSchema={validationSchema} onSubmit={handleSubmit}>
      <Form>
        <Heading fontSize="2xl" marginBottom={8}>Sign In to Your Account</Heading>
        <Stack spacing={4}>
          <FormInput label="Email" type="email" name="email"/>
          <FormInput label="Password" type="password" name="password"/>
          <Button type="submit">Sign In</Button>
          <AlertBox alert={alert} clearAlert={clearAuthAlert}/>
        </Stack>
      </Form>
    </Formik>
  )
}

export default LoginForm