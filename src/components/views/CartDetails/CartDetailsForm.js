// TODO: replace formik with react-hook-form
// https://blog.logrocket.com/react-hook-form-vs-formik-comparison/

import React, { useEffect, useState } from 'react'
import {
  Box,
  FormControl,
  FormErrorMessage,
  FormLabel,
  Heading,
  IconButton,
  Input,
  InputGroup,
  InputLeftAddon,
  InputRightElement,
  Stack,
  VStack,
} from '@chakra-ui/react'
import { Form, Formik } from 'formik'
import * as Yup from 'yup'
import { useNavigate } from 'react-router-dom'
import { useDispatch, useSelector } from 'react-redux'
import { AddIcon } from '@chakra-ui/icons'
import FormInput from '../../common/FormInput'
import FormTextArea from '../../common/FormTextArea'
import TransactionDetailsTable from './TransactionDetailsTable'
import { routes } from '../../../constants/routes'
import { addCart, updateCart } from '../../../store/carts'
import { addItem, clearCartItems, fetchCartItems } from '../../../store/cartItems'

function CartDetailsForm ({ documentNumber, data, edit }) {
  const dispatch = useDispatch()
  const navigate = useNavigate()

  const products = useSelector(state => state.products.data)
  const items = useSelector(state => state.cartItems.data)
  const [productCode, setProductCode] = useState('')
  const [addItemMessage, setAddItemMessage] = useState('')

  const productAmount = items.length
  const quantityAmount = items.reduce((total, item) => total + item.quantity, 0)
  const totalGrossPrice = items.reduce((total, item) => total + item.quantity * (item.product?.price || 0), 0)
  const netPrice = totalGrossPrice - (data?.totalLineDiscount || 0)

  useEffect(() => {
    if (documentNumber) {
      if (!data) {
        navigate(routes.CART)
      } else {
        dispatch(fetchCartItems(documentNumber))
      }
    }
    return function () {
      dispatch(clearCartItems())
    }
  }, [data, dispatch, documentNumber, navigate])

  const initialValues = {
    documentNumber: '-',
    dateCreated: new Date().toISOString().split('T')[0],
    description: '',
    ...data,
  }

  function handleProductCodeChange (event) {
    setProductCode(event.target.value)
  }

  const handleKeyDown = (event) => {
    if (event.key === 'Enter') {
      event.preventDefault()
      handleAddItem()
    }
  }

  function handleAddItem () {
    if (!productCode) return
    const newProduct = products.find(product => product.code.toUpperCase() === productCode.toUpperCase())
    setAddItemMessage('')
    if (newProduct) {
      dispatch(addItem(newProduct))
      setProductCode('')
    } else {
      setAddItemMessage('Product not found!')
    }
  }

  const validationSchema = Yup.object({
    description: Yup.string().max(255, 'Max 255 characters'),
  })

  function handleSubmit (values) {
    if (documentNumber) {
      dispatch(updateCart(values))
      navigate(`${routes.CART}/${documentNumber}/view`)
    } else {
      dispatch(addCart(values))
      navigate(routes.CART)
    }
  }

  function handleReset () {
    if (documentNumber) {
      dispatch(fetchCartItems(documentNumber))
      navigate(`${routes.CART}/${documentNumber}/view`)
    } else {
      navigate(routes.CART)
    }
  }

  return (
    <Box overflow="scroll" borderWidth="2px" rounded="lg" padding={2}>
      <Formik initialValues={initialValues} validationSchema={validationSchema} onSubmit={handleSubmit} onReset={handleReset}>
        {/*{formik => (*/}
        <Form id="cart-form">
          <fieldset disabled={!edit}>
            <VStack gridRowGap={4}>
              <VStack width="full" borderWidth="1px" rounded="lg" padding={2}>
                <Heading size="lg" marginBottom={2}>TRANSACTION HEADER</Heading>
                <Stack direction={['column', 'column', 'row']} width="full">
                  <VStack>
                    <FormInput
                      type="text"
                      name="documentNumber"
                      label="Invoice Number"
                      disabled={!documentNumber}
                      readOnly
                    />
                    <FormInput type="date" name="dateCreated" label="Order Date" disabled={!documentNumber} readOnly/>
                  </VStack>
                  <FormTextArea
                    name="description"
                    label="Description"
                    minHeight="7.5rem"
                    resize="none"
                    // sx={{ '& textarea': { 'minHeight': '7.5em', 'resize': 'none' } }}
                    readOnly={!edit}
                  />
                </Stack>
              </VStack>

              {/* todo: responsive */}
              <VStack width="full" borderWidth="1px" rounded="lg" padding={2}>
                <Heading size="lg" marginBottom={2}>TRANSACTION TOTAL</Heading>
                <Stack direction={['column', 'row']} width="full">
                  <VStack width="full">
                    <FormControl>
                      <FormLabel>Total Gross Price</FormLabel>
                      <InputGroup>
                        <InputLeftAddon children="Rp"/>
                        <Input value={totalGrossPrice || 0} readOnly/>
                      </InputGroup>
                    </FormControl>
                    <FormControl>
                      <FormLabel>Total Line Discount</FormLabel>
                      <InputGroup>
                        <InputLeftAddon children="Rp"/>
                        <Input value={data?.totalLineDiscount || 0} readOnly/>
                      </InputGroup>
                    </FormControl>
                    <FormControl>
                      <FormLabel>Net Price</FormLabel>
                      <InputGroup>
                        <InputLeftAddon children="Rp"/>
                        <Input value={netPrice || 0} readOnly/>
                      </InputGroup>
                    </FormControl>
                  </VStack>
                  <VStack width="full">
                    <FormControl>
                      <FormLabel>Product Amount</FormLabel>
                      <Input value={productAmount || 0} readOnly/>
                    </FormControl>
                    <FormControl>
                      <FormLabel>Quantity Amount</FormLabel>
                      <Input value={quantityAmount || 0} readOnly/>
                    </FormControl>
                  </VStack>
                </Stack>
              </VStack>

              <VStack width="full" borderWidth="1px" rounded="lg" padding={2}>
                <Heading size="lg" marginBottom={2}>TRANSACTION DETAILS</Heading>
                <TransactionDetailsTable edit={edit}/>
                {edit &&
                  <FormControl width="15rem" isInvalid={addItemMessage}>
                    <InputGroup>
                      <Input
                        placeholder="Product Code"
                        value={productCode}
                        textTransform="uppercase"
                        onChange={handleProductCodeChange}
                        onKeyDown={handleKeyDown}
                      />
                      <InputRightElement padding={0}>
                        <IconButton
                          icon={<AddIcon/>}
                          aria-label="Add Product"
                          borderLeftRadius={0}
                          onClick={handleAddItem}
                        />
                      </InputRightElement>
                    </InputGroup>
                    <FormErrorMessage>{addItemMessage}</FormErrorMessage>
                  </FormControl>
                }
              </VStack>
            </VStack>
            {/*/!*for development *!/*/}
            {/*<Box as='pre' marginTop={10} overflow='hidden' textOverflow='ellipsis'>*/}
            {/*  "values": {JSON.stringify(formik.values, null, 2)}*/}
            {/*  <br/>*/}
            {/*  "errors": {JSON.stringify(formik.errors, null, 2)}*/}
            {/*</Box>*/}
          </fieldset>
        </Form>
        {/*)}*/}
      </Formik>
    </Box>
  )
}

export default CartDetailsForm
