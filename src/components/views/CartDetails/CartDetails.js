import React, { useRef } from 'react'
import { Link, useNavigate, useParams } from 'react-router-dom'
import { useDispatch, useSelector } from 'react-redux'
import {
  AlertDialog,
  AlertDialogBody,
  AlertDialogContent,
  AlertDialogFooter,
  AlertDialogHeader,
  AlertDialogOverlay,
  Button,
  ButtonGroup,
  Stack,
  useDisclosure,
} from '@chakra-ui/react'
import Header from '../../common/Header'
import AlertBox from '../../common/AlertBox'
import CartDetailsForm from './CartDetailsForm'
import { routes } from '../../../constants/routes'
import { checkCartPromotions, checkoutCart, clearCartAlert } from '../../../store/carts'

function CartDetails ({ edit }) {
  const dispatch = useDispatch()
  const navigate = useNavigate()
  const { isOpen, onOpen, onClose } = useDisclosure()
  const cancelRef = useRef()
  const { documentNumber } = useParams()
  const data = useSelector(state => state.carts.data.content?.find(cart => cart.documentNumber === documentNumber))
  const alert = useSelector(state => state.carts.alert)

  function handleCheckPromotions () {
    dispatch(checkCartPromotions(documentNumber))
  }

  function handleCheckout () {
    dispatch(checkoutCart(documentNumber))
    onClose()
  }

  function handlePrint () {
    window.open(`http://localhost:8080/carts/${documentNumber}/pdf`, '_blank')
    return false
  }

  function handleEdit () {
    dispatch(clearCartAlert())
    navigate(`${routes.CART}/${documentNumber}/edit`)
  }

  return (
    <Stack id="app">
      <Header index={2}/>
      <Stack isInline justifyContent="space-between" alignItems="flex-end">
        <Link to={routes.CART}>
          <Button>Back</Button>
        </Link>
        {edit ? (
          <ButtonGroup key="1" colorScheme="blue">
            <Button form="cart-form" type="submit" width="4rem">Save</Button>
            <Button form="cart-form" type="reset" width="4rem" variant="outline">Cancel</Button>
          </ButtonGroup>
        ) : (
          <ButtonGroup key="2">
            <Button width="6rem" onClick={handleCheckPromotions} disabled={data?.isPaid}>Get Promo</Button>
            <Button width="6rem" onClick={onOpen} disabled={data?.isPaid}>Checkout</Button>
            <Button width="4rem" onClick={handlePrint}>Print</Button>
            <Button width="4rem" onClick={handleEdit} disabled={data?.isPaid}>Edit</Button>
          </ButtonGroup>
        )}
      </Stack>
      <AlertBox alert={alert} clearAlert={clearCartAlert}/>
      <CartDetailsForm documentNumber={documentNumber} data={data} edit={edit}/>
      <AlertDialog isOpen={isOpen} leastDestructiveRef={cancelRef} onClose={onClose}>
        <AlertDialogOverlay>
          <AlertDialogContent>
            <AlertDialogHeader fontSize="lg" fontWeight="bold">
              Checkout "{data?.documentNumber}"
            </AlertDialogHeader>
            <AlertDialogBody>
              Are you sure? Cart can't be updated after checkout.
            </AlertDialogBody>
            <AlertDialogFooter>
              <Button ref={cancelRef} onClick={onClose}>
                Cancel
              </Button>
              <Button colorScheme="teal" onClick={handleCheckout} ml={3}>
                OK
              </Button>
            </AlertDialogFooter>
          </AlertDialogContent>
        </AlertDialogOverlay>
      </AlertDialog>
    </Stack>
  )
}

export default CartDetails