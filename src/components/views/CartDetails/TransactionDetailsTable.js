import React from 'react'
import { useDispatch, useSelector } from 'react-redux'
import { ButtonGroup, IconButton, Table, TableContainer, Tbody, Td, Th, Thead, Tooltip, Tr } from '@chakra-ui/react'
import { AddIcon, DeleteIcon, MinusIcon } from '@chakra-ui/icons'
import { addItemQty, deleteItem, subtractItemQty } from '../../../store/cartItems'
import TableRowNoData from '../../common/TableRowNoData'

function TransactionDetailsTable ({ edit }) {
  const dispatch = useDispatch()
  const data = useSelector(state => state.cartItems.data)

  function displayPromotion (promotion) {
    switch (promotion?.type) {
      case 'DISCOUNT':
        return `${promotion.discount}% discount (max ${promotion.discountLimit.toLocaleString('id-ID', {
          style: 'currency',
          currency: 'IDR',
        })})`
      case 'FREE_GOODS':
        return `Free ${promotion.freeProductCode} (x${promotion.freeProductQuantity})`
      default:
        return '-'
    }
  }

  return (
    <TableContainer width="full" borderWidth={2} borderRadius={8}>
      <Table size="sm">
        <Thead>
          {data.length === 0 ? <TableRowNoData/> : (
            <Tr>
              <Th>Product</Th>
              <Th isNumeric>Stock</Th>
              <Th isNumeric>Amount</Th>
              <Th isNumeric>Price</Th>
              <Th isNumeric>Total Price</Th>
              <Th>Promotion</Th>
              {edit && <Th>Actions</Th>}
            </Tr>
          )}
        </Thead>
        <Tbody>
          {data.map(item => (
            <Tr key={item.product?.code || item.id}>
              <Td>{item.product?.name}</Td>
              <Td isNumeric>{item.product?.stock}</Td>
              <Td isNumeric>{item.quantity}</Td>
              <Td isNumeric>{item.product?.price.toLocaleString('id-ID', { style: 'currency', currency: 'IDR' })}</Td>
              <Td isNumeric>{(item.product?.price * item.quantity).toLocaleString('id-ID', {
                style: 'currency',
                currency: 'IDR',
              })}</Td>
              <Td>
                <Tooltip label={`${item.promotion?.documentNumber}: ${item.promotion?.description}`}>
                  {displayPromotion(item.promotion)}
                </Tooltip>
              </Td>
              {edit &&
                <Td width="8rem">
                  <ButtonGroup size="sm">
                    <Tooltip label={' Add Item'}>
                      <IconButton
                        icon={<AddIcon/>}
                        aria-label="Add Item"
                        onClick={() => dispatch(addItemQty(item.product?.code))}
                      />
                    </Tooltip>
                    <Tooltip label="Subtract Item">
                      <IconButton
                        icon={<MinusIcon/>}
                        aria-label="Subtract Item"
                        onClick={() => dispatch(subtractItemQty(item.product?.code))}
                      />
                    </Tooltip>
                    <Tooltip label="Delete Item">
                      <IconButton
                        icon={<DeleteIcon color="red.500"/>}
                        aria-label="Delete Item"
                        onClick={() => dispatch(deleteItem(item.product?.code))}
                      />
                    </Tooltip>
                  </ButtonGroup>
                </Td>
              }
            </Tr>
          ))}
        </Tbody>
      </Table>
    </TableContainer>
  )
}

export default TransactionDetailsTable