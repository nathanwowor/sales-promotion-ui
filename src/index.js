import React from 'react'
import ReactDOM from 'react-dom'
import { Provider } from 'react-redux'
import { ChakraProvider } from '@chakra-ui/react'
import './index.css'
import App from './App'
import store from './store'

// redux store: connect few parent component vs every child component
// https://redux.js.org/faq/react-redux#should-i-only-connect-my-top-component-or-can-i-connect-multiple-components-in-my-tree

ReactDOM.render(
  <React.StrictMode>
    <Provider store={store}>
      <ChakraProvider>
        <App/>
      </ChakraProvider>
    </Provider>
  </React.StrictMode>,
  document.getElementById('root'),
)
