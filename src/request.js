import axios from 'axios'
import { routes } from './constants/routes'

const instance = axios.create({
  baseURL: 'http://localhost:8080/',
  headers: {
    'Authorization': `Bearer ${localStorage.getItem('token')}`,
  },
})

instance.interceptors.response.use(
  function (response) {
    return response
  },
  function (error) {
    if (error.response.status === 401) {
      localStorage.removeItem('token')
      window.location.assign(routes.LOGIN)
      alert('Session expired, please login again.')
    }
    return Promise.reject(error)
  },
)

export default instance