import React from 'react'
import { BrowserRouter, Route, Routes } from 'react-router-dom'
import { routes } from './constants/routes'
import Home from './components/views/Home'
import Login from './components/views/Login'
import Cart from './components/views/Cart'
import Promotion from './components/views/Promotion'
import CartDetails from './components/views/CartDetails'
import PromotionDetails from './components/views/PromotionDetails'

function App () {
  return (
    <BrowserRouter>
      <Routes>
        <Route path={routes.HOME} element={<Home/>}/>
        <Route path={routes.CART} element={<Cart/>}/>
        <Route path={routes.PROMOTION} element={<Promotion/>}/>
        <Route path={routes.CART + '/new'} element={<CartDetails edit={true}/>}/>
        <Route path={routes.CART + '/:documentNumber/edit'} element={<CartDetails edit={true}/>}/>
        <Route path={routes.CART + '/:documentNumber/view'} element={<CartDetails edit={false}/>}/>
        <Route path={routes.PROMOTION + '/new'} element={<PromotionDetails edit={true}/>}/>
        <Route path={routes.PROMOTION + '/:documentNumber/edit'} element={<PromotionDetails edit={true}/>}/>
        <Route path={routes.PROMOTION + '/:documentNumber/view'} element={<PromotionDetails edit={false}/>}/>
        <Route path={routes.LOGIN} element={<Login/>}/>
      </Routes>
    </BrowserRouter>
  )
}

export default App
